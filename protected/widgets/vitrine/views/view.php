<div class="b-vitrine">
<?php
	$count = count($models);
	if ($count > 1) {
?>
<ul>
<?php
	}
	
	$html = '';
	$i = 0;
	foreach ($models as $model) {
	  $id    = $model->id_vitrine;
	  $class = ($html == '') ? 'style="display:block"' : '';
	  $link  = $model->link;
	  //$activeClass = ($html == '') ? ' class="cur"' : '';
	  if($html == '' )  {$activeClass = ' class="cur"'; } else {$activeClass = ''; }
	  
	  $src="";
	  $preview = $model->getImagePreview('_list');
	  if($preview) {
	  	$src = $preview->getUrlPath();
	  }

	  if ($count > 1) {
?>
	<li <?= $activeClass; ?> ><a rel="<?= $i;?>" href="#"><?php $i++; echo $i; ?></a></li>
<?php
	  }
	  
	  $html .= '<img '.$class.' class="b-vitrine__slide" src="'.$src.'">';
	}

	if ($count > 1) {
?>
  </ul>
<?php
	}
	
	echo $html;
?>
</div>