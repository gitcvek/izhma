<?php
	class ReCaptcha extends CApplicationComponent {
		public $sitekey;
		public $secretkey;
		public $url = 'https://www.google.com/recaptcha/api/siteverify';
		
		public function isGoodCaptcha($response) {
			$url = $this->url;
			$secret = $this->secretkey;
	
			$remoteip = CHttpRequest::getUserHostAddress();
			 
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secret, 'response' => $response, 'remoteip' => $remoteip)));
			$out = curl_exec($ch);
			 
			$resp = json_decode($out, true);
			 
			$save = is_array($resp) && array_key_exists('success', $resp) && (((bool) $resp['success']) == true);

			return $save;
		}
	}