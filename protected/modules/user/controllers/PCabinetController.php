<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 24.03.14
 * Time: 12:38
 */
Yii::import('ygin.modules.user.controllers.CabinetController');
class PCabinetController extends CabinetController {

  public function actionRegister() {
//    $this->module->redirectRouteAfterRegister = '/index.php';
//    return parent::actionRegister();
    $model = BaseActiveRecord::newModel('User', 'register');
    $modelClass = get_class($model);
    $this->performAjaxValidation($model, 'register-form');

    if (isset($_POST[$modelClass])) {
      $model->attributes = $_POST[$modelClass];
      //Создаем indentity раньше сохранения модели
      //т.к. после сохранения поле user_password измениться на хеш
      $identity = new UserIdentity($model->name, $model->user_password);
      $model->onAfterSave = array($this, 'sendRegisterMessage');
      if ($model->save()) {
        //если разрешено сразу авторизовать пользователя
        if (Yii::app()->getModule('user')->immediatelyAuthorization) {
          //загружаем модель пользователя
          $identity->authenticate();
          //Сразу авторизуем пользователя
          Yii::app()->user->login($identity);
          Yii::app()->user->setFlash('registerSuccess', 'Регистрация успешно завершена.');
        } else {
          Yii::app()->user->setFlash('registerSuccess', 'Регистрация успешно завершена. Теперь вы можете войти на сайт через форму авторизации.');
        }

        $this->redirect('/');
      }
    }
    $this->render('/register', array('model' => $model));
  }

  public function actionLogin() {
    $model = BaseFormModel::newModel('LoginForm');
    $modelClass = get_class($model);
    $ajaxId = isset($_POST['login-widget-form']) ?'login-widget-form' : 'login-form';
    if (isset($_POST['ajax']) && $_POST['ajax']===$ajaxId) {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
    if (isset($_POST[$modelClass])) {
      $model->attributes = $_POST[$modelClass];
      if ($model->validate() && $model->login()) {
        Yii::log('Успешная авторизация под логином '.$model->username, CLogger::LEVEL_INFO, 'application.login.success');
        $this->redirect('/');
      } else {
        Yii::log('Ошибка авторизации (логин='.$model->username.')', CLogger::LEVEL_INFO, 'application.login.error');
        Yii::app()->user->setFlash('user_login_error', CHtml::errorSummary($model, ' '));
      }
    }

    $view = '/login';
    if (Yii::app()->isBackend) $view = 'backend.views.auth';
    if ($view != null) {
      $this->render($view, array('model'=>$model));
    } else {
      $this->redirect('/');
    }
  }


} 