<?php
Yii::import('ygin.modules.photogallery.controllers.PhotogalleryController');
class PPhotogalleryController extends PhotogalleryController {

  
  public function actionView($idGallery) {
    $currentGallery = $this->loadModelOr404('Photogallery', $idGallery);
    $this->setKeywords($currentGallery->meta_keywords);
    $this->setDescription($currentGallery->meta_description);
    
    $childGallery = Photogallery::model()->byParent($currentGallery->id_photogallery)->with('countPhoto')->findAll();
    
    $arr = array();
    $tmp = $currentGallery;
    while($tmp != null) {
      $arr[$tmp->name] = $tmp->getUrl();
      $tmp = $tmp->getParent();
    }
    if (count($arr) > 0) {
      $this->breadcrumbs = array_merge($this->breadcrumbs, array_reverse($arr));
    }
    
    $this->render('/photogallery', array(
      'currentGallery' => $currentGallery,
      'childGallery' => $childGallery,
    ));
    
  }
  
}