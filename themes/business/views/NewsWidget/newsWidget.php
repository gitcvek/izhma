<?php
if ($this->beginCache( __FILE__.__LINE__.'_newsWidget'.$url , array(
    'dependency'=> array(
        'class' => 'system.caching.dependencies.CDbCacheDependency',
        'sql'   => 'SELECT MAX(r.date_update) FROM (
                  	  SELECT MAX(p.date_update) as date_update FROM pr_news p
                	  UNION
                 	  SELECT MAX(c.date_update) as date_update FROM pr_news_category c
             	       ) r',
    ),
    'duration' => 3600*24*365, // Очень большое число, чтобы кеш не зависел от времени
    'varyByParam' => array('page'), // Гет-параметры (например, пагинация)
))) {
	Yii::app()->clientScript->registerCssFile('themes/business/views/NewsWidget/assets/newsWidget.css');
	
	$news = $this->getNews();
	
	if (count($news)) {
?>
<div class="mNewsList">
  <h2><?php echo Yii::t('common', 'Новости');?></h2>
<?php
  //если нужно брать переопределенное представление элемента списка из темы,
  //то вместо news.views._list_item нужно прописать
  //webroot.themes.название_темы.views.news._list_item

  foreach ($news as $model):
      if ($model->getShort())  {
        $this->render('webroot.themes.business.views.news._list_item', array('model' => $model)); 
      }
  endforeach;
?>
<div class="archive"><a href="<?php echo Yii::app()->createUrl(NewsModule::ROUTE_NEWS_CATEGORY);?>">Все новости</a> &rarr;</div>
</div>
<?php
	}
	
	$this->endCache();
	
	if (YII_DEBUG) {
        echo "<div class='label label-danger'>Страница НЕ из кеша</div>";
    }
} else {
    if (YII_DEBUG){
    	echo "<div class='label label-success'>Страница из кеша</div>";
    }
}
?>