<?php
Yii::app()->clientScript->registerScript('vitrine', '
  !function() {
    var $carousel = $("#carousel").carousel({
      indicator: true
    });
    
    $(".carousel_indicator").appendTo("#head");
    
    $(".carousel_indicator span").on("click", function() {
      clearInterval(interval);
      var $this = $(this);
      var index = $this.index();
      var activeIndex = $carousel.carousel("getCurrent");
      if (index > activeIndex) {
        for(var i = 0; i < (index - activeIndex); i++) {
          $carousel.carousel("next");
        }
      } else {
        if (index < activeIndex) {
          for(var i = 0; i < (activeIndex - index); i++) {
            $carousel.carousel("prev");
          }
        }
      }
      interval = setInterval(fade, 6000);
    });
    
    var fade = function() {
      $carousel.carousel("next")
    }
    var interval = setInterval(fade, 6000);
  }();
 ', CClientScript::POS_READY);
?>
<div class="tabbable tabs-below carousel-wrapper">
<div class="carousel">
  <div id="carousel" class="carousel_inner tab-content">
<?php
$html = '';
foreach ($models AS $model) {
 $id    = $model->id_vitrine;
  $class = ($html == '') ? 'tab-pane hero-unit carousel_box active' : 'tab-pane hero-unit carousel_box';
  $link  = $model->link;
  $activeClass = ($html == '') ? ' class="active"' : '';
  $img = '';
  if ($model->file) {
  	$img = $model->file->getUrlPath();
  }
?>
    <div  class="<? echo $class ?>" style="background-image:url(<?= $img ?>)">
      <div><a href="<?php echo $link; ?>"><?php echo $model->text; ?></a></div>
<?php /*     <p><a href="<?php //echo $link; ?>" class="btn btn-info btn-large">Узнать больше »</a></p> */?>
    </div>
<?php
  $html .= '<li'.$activeClass.'><a href="#id-'.$id.'" data-toggle="tab">'.$model->title.'</a></li>';
}
?>
  </div>
</div>
<div class="fon">
</div>
