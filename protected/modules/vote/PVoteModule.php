<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 24.03.14
 * Time: 10:24
 */
Yii::import('ygin.modules.vote.VoteModule');
class PVoteModule extends VoteModule {

  public function init() {
    $this->defaultController = 'voting';

    $this->setImport(array(
      'application.modules.vote.models.UserVote',
      'ygin.modules.vote.models.*',
      'ygin.models.VisitSite',
      'ygin.modules.vote.controllers.*',
      'ygin.modules.vote.widgets.VoteWidget',
    ));

    Yii::app()->setComponent('vote', $this);
  }

  //public function check($idVote) {
  //  //чекаем только по логину
  //  $voted = UserVote::model()->findAll(array(
  //    'condition' => "id_user = :idu AND id_vote = :idv",
  //    'params' => array(
  //      ':idu' => Yii::app()->user->id,
  //      ':idv' => $idVote
  //    )
  //  ));
  //  if($voted) {
  //    return False;
  //  } else {
  //    return True;
  //  }
  //}

} 