<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 24.03.14
 * Time: 10:12
 */
Yii::import('ygin.modules.vote.controllers.VotingController');
Yii::import('application.modules.vote.models.UserVote');
class PVotingController extends VotingController {

  public function actionVote() {
    //if(Yii::app()->user->isGuest) {
    //  $this->redirect('/login');
    //}
    $voting = null;
    if (is_numeric(HU::post('id_voting'))) {
      $voting = Voting::model()->onlyActive()->with('answer')->findByPk(HU::post('id_voting'));
    }
    if ($voting == null) {
      //echo CHtml::encode($this->controller->widget('vote.widgets.VoteWidget', null, true));
      return;
    }

    if (Yii::app()->vote->check($voting->id_voting)) {
      $answers = $_POST['VotingAnswer']['name'];
      $cr = new CDbCriteria();
      $cr->addColumnCondition(array('id_voting' => $voting->id_voting));
      if(is_array($answers)) {
        $cr->addInCondition('id_voting_answer', $answers);
      } else if (is_numeric($answers)) {
        $cr->addColumnCondition(array('id_voting_answer' => $answers));
      }
      VotingAnswer::model()->updateCounters(array('count'=>1), $cr);

      //добавляем запись в таблицу pr_link_user_voting
      $userVote = new UserVote;
      $userVote->id_user = Yii::app()->user->id;
      $userVote->id_vote = HU::post('id_voting');
      $userVote->save(false);

      VisitSite::saveCurrentVisit(Voting::ID_OBJECT, $voting->id_voting);
      Yii::app()->user->setState('vote_'.$voting->id_voting, time());

      // перегружаем голосовалку, чтоб обновились показатели счетчиков
      $voting = Voting::model()->onlyActive()->with('answer')->findByPk($voting->id_voting);
    }

    $voteCount = $voting->getSumVote();

    echo CHtml::encode($this->renderPartial("application.modules.vote.widgets.views.statistic",array(
      'voting'=>$voting,
      'voteCount'=>$voteCount,
    )), null, true);
  }

} 