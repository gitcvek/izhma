<?php

/**
 * This is the model class for table "pr_link_document_type".
 *
 * The followings are the available columns in table 'pr_link_document_type':
 * @property integer $id_documents
 * @property integer $id_documents_type
 */
class LinkDocumentType extends CActiveRecord
{
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return LinkDocumentType the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'pr_link_document_type';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('id_documents, id_documents_type', 'numerical', 'integerOnly'=>true),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_documents' => 'Id Documents',
      'id_documents_type' => 'Id Documents Type',
    );
  }

}