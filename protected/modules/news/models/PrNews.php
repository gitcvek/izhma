<?php

/**
 * This is the model class for table "pr_news".
 *
 * The followings are the available columns in table 'pr_news':
 * @property integer $id_news
 * @property string $title
 * @property string $date
 * @property integer $id_news_category
 * @property string $short
 * @property string $short_km
 * @property string $content
 * @property integer $photo
 * @property integer $is_visible
 */
class PrNews extends News
{   
    const SCENARIO_UPDATE_VIEWS = 'views';
    public $date_create;
    public $date_update;
    

    const EMPTY_IMAGE = '/themes/business/gfx/all_doc_list.jpg';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getTitle()
    {
        return $this->getAttribute('title_' . Yii::app()->language);
    }

    public function getShort()
    {
        return $this->getAttribute('short_' . Yii::app()->language);
    }

    public function getContent()
    {
        return $this->getAttribute('content_' . Yii::app()->language);
    }

    public function rules()
    {
        $newArray = array();
        $array = array(
            array('title, date, content', 'required'),
            array('id_news_category, photo, is_visible', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('date, date_create, date_update', 'length', 'max' => 10),
            array('date_create, date_update, short', 'safe'),
            array('content_ru', 'required', 'on'=>self::SCENARIO_UPDATE_VIEWS),
        );

        foreach ($array AS $cur) {
            $cur[0] = str_replace('title', 'title_ru', $cur[0]);
            
            $cur[0] = str_replace('short', 'short_ru', $cur[0]);
            $newArray[] = $cur;
        }
        return $newArray;
    }

    public function behaviors()
    {
        $newBehavior = array(
            'ImagePreviewBehavior' => array(
                'class' => 'ImagePreviewBehavior',
                'imageProperty' => 'image',
                'formats' => array(
                    '_hot' => array(
                        'width' => 422,
                        'height' => 297,
                    ),
                    '_newsWidget' => array(
                        'width' => 213,
                        'height' => 0,
                    ),
                ),
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_create',
                'updateAttribute' => 'date_update',
                'setUpdateOnCreate' => true,
            ),
        );

        return CMap::mergeArray(parent::behaviors(), $newBehavior);
    }

    public function defaultScope()
    {
        if (Yii::app() instanceOf BackendApplication) return array();
        $alias = $this->getTableAlias(true, false);
        return array(
            'condition' => "$alias.is_visible = 1",
        );
    }

    public function getKeywords()
    {
        return $this->meta_keywords;
    }

    public function getDescription()
    {
        return $this->meta_description;
    }

    public static function getDependency()
    {
        return new CDbCacheDependency('SELECT MAX(r.date_update) FROM (
		SELECT MAX(p.date_update) as date_update FROM pr_news p
		UNION
		SELECT MAX(c.date_update) as date_update FROM pr_news_category c
	) r');
    }

    protected function beforeSave()
    {
        $content_ru = $this->content_ru;
        $content_komi = $this->content_kmi;
        $pattern = "/<img ([^>]*)base64([^>]*)>/";
        if (stristr($content_ru, 'base64') == true) {
            $this->content_ru = preg_replace($pattern, "", $content_ru);
        }
        if (stristr($content_komi, "base64") == true) {
            $this->content_kmi = preg_replace($pattern, "", $content_komi);
        }
        return parent::beforeSave();
    }
}