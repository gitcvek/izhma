<?php

/**
 * This is the model class for table "pr_link_user_vote".
 *
 * The followings are the available columns in table 'pr_link_user_vote':
 * @property integer $id_user
 * @property string id_vote
 */
class UserVote extends DaActiveRecord {
  
 /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return UserVoteLink the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'pr_link_user_voting';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    return array(
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
    );
  }
  
  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_user' => 'id user',
      'id_vote' => 'id vote',
    );
  }

}