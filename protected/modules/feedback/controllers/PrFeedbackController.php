<?php
Yii::import('ygin.modules.feedback.controllers.FeedbackController');
Yii::import('application.modules.feedback.models.Adresat');

class PrFeedbackController extends FeedbackController  {
	private $mail = null;

	public function actionIndex() {
		$model = BaseActiveRecord::newModel('Feedback');
		$modelClass = get_class($model);
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'feedbackForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST[$modelClass])) {
			$model->attributes=$_POST[$modelClass];
			if (isset($_POST['g-recaptcha-response']) && Yii::app()->reCaptcha->isGoodCaptcha($_POST['g-recaptcha-response'])) {
			    
			    //отправка мейла
				if (is_numeric($model->adresat)) {
				 $id = intval($model->adresat);
				 $adresat = Adresat::model()->findByPk($id);
				 if ($adresat != null) {
				   $model->adresat = $adresat->name;
				   $this->mail = $adresat->email;
				 }
				}
				
	
				$model->Upload = CUploadedFile::getInstance($model,'fileupload');
	
				$model->onAfterSave = array($this, 'sendMessage'); //Регистрируем обработчик события
	
				if ($model->save()) {
					if ($model->Upload)  {
						$droot = Yii::getPathOfAlias('webroot');
						$id_feedback = $model->primaryKey;
						if (!file_exists($droot.'/content/feedback/'.$id_feedback)) {
							if (mkdir($droot.'/content/feedback/'.$id_feedback, 0777)) {
								chmod($droot.'/content/feedback/'.$id_feedback, 0777);
							}
						}
						$fName = HText::translit($_FILES['PrFeedback']['name']['fileupload'], "-");
						$model->Upload->saveAs($droot.'/content/feedback/'.$id_feedback.'/'.$fName);
	
						$file = new File();
						$file->file_path = 'content/feedback/'.$id_feedback.'/'.$fName;
						$file->id_object = 517;
						$file->id_instance = $id_feedback;
						$file->id_parameter = 7502;
						$file->save();
	
						$model->fileupload = $file->id_file;
						$model->save();
					}
					
					Yii::app()->user->setFlash('feedback-success', 'Спасибо за обращение. Ваше сообщение успешно отправлено.');
				} else {
					// вообще сюда попадать в штатных ситуациях не должны
					// только если кул хацкер резвится
					Yii::app()->user->setFlash('feedback-message', CHtml::errorSummary($model, '<p>Не удалось отправить форму</p>'));
				}
			} else {
        		Yii::app()->user->setFlash('feedback-message', 'Подтвердите, пожалуйста, что Вы не робот.');
        	}
		}
		$this->redirect(Yii::app()->user->returnUrl);
	}



	public function sendMessage(CEvent $event) {
		/**
		 * @var Feedback $model
		 */
		$model = $event->sender;
		$message = '';
		$default = true;

		if ($this->module->hasEventHandler('onFormMessage')) {
			$formMsgEvent = new CEvent($model);
			$this->module->onFormMessage($formMsgEvent);
			$message = HArray::val($formMsgEvent->params, 'message');
			//если сообщение пришло пустое
			//то формируем ссобщение по-умолчанию
			$default = empty($message);
		}
		if ($default) {
			$link='';
			if ($model->fileupload)  {
				$link =  'Ссылка на файл: '.$_SERVER['HTTP_HOST'].'/content/feedback/'.$model->id_feedback.'/'.HText::translit($_FILES['PrFeedback']['name']['fileupload'], "-")."\n";
			}

			$tmpl = "Отправлено новое сообщение через форму обратной связи:\n".
          "для кого: {to}\n".
          "время: {time}\n".
          "имя: {name}\n".
          "тема: {themes}\n".
          "телефон: {phone}\n".
          "e-mail: {email}\n".
          "Текст сообщения:\n{msg}\n". 
			$link.
          "---\n";

			$message = strtr($tmpl, array(
          '{time}'  => date('d.m.Y H:i', $model->date),
          '{name}'  => $model->fio,
          '{themes}'  => $model->themes,
          '{phone}' => $model->phone,
          '{email}' => $model->mail,
          '{msg}'   => $model->message,
          '{to}'   => $model->adresat,
			));
		}
		
		    Yii::app()->notifier->addNewEvent(Yii::app()->getModule('feedback')->idEventType, $message, 512, $this->mail);	//id subscriber указан как константа	
		    
	}
}
