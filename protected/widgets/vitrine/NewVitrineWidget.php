<?php
Yii::import('application.widgets.vitrine.models.Vitrine');

class NewVitrineWidget extends DaWidget {
  public function run() {
    $models = Vitrine::model()->findAll(array('condition' => 'visible = 1'));
    $this->render("application.widgets.vitrine.views.view", array("models" => $models) );
  }
}