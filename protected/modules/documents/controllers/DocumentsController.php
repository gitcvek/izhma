<?php

class DocumentsController extends Controller
{
protected $urlAlias = "documents";
  public function actionIndex()
  {
    $categoryList = DocumentsType::model()->with('countDocs')->findAll();

    $criteria = new CDbCriteria();
    $criteria->order = 'date DESC';

    $count = Documents::model()->count();

    $pages = new CPagination($count);
    $pages->pageSize = 10;
    $pages->applyLimit($criteria);
    
    $docList = Documents::model()->findAll($criteria);

    $this->render('/index', array(
      'docList' => $docList,
      'categoryList' => $categoryList,
      'pages' => $pages,  // пагинатор
    ));
  }
  
  public function actionCategory($id)
  {
    $criteria = new CDbCriteria(array(
          'with' => array(
            'category' => array(
                 'together' => true,
                 'condition' => 'category_category.id_documents_type = '.$id,
                ) ,
          ),
    ));
    $criteria->order = 'date DESC';
    //$criteria->addInCondition('category.id_documents_type', $id);
    
    $categoryList = DocumentsType::model()->with('countDocs')->findAll();

    $count = Documents::model()->count($criteria);

    $pages = new CPagination($count);
    $pages->pageSize = 10;
    $pages->applyLimit($criteria);
    
    $docList = Documents::model()->findAll($criteria);

    $this->render('/index', array(
      'docList' => $docList,
      'categoryList' => $categoryList,
      'pages' => $pages,  // пагинатор
    ));
  }

}
