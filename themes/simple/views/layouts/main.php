<!DOCTYPE html>
<html>
<head>
<!--[if lt IE 7]>
<meta http-equiv="Refresh" content="0;URL=/ie6/index_ru.html">
<![endif]-->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
  <meta http-equiv="content-language" content="ru" > 
  <meta http-equiv="pragma" content="no-cache">
  <meta name="msapplication-window" content="width=1024;height=768">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7, IE=9" />
<?php 
  //Регистрируем файлы скриптов в <head> 
  Yii::app()->clientScript->registerCoreScript('jquery');
  Yii::app()->clientScript->registerScriptFile('/engine/lib/bootstrap/js/bootstrap.min.js', CClientScript::POS_HEAD);
  Yii::app()->clientScript->registerScriptFile('/js/js.js', CClientScript::POS_HEAD);
  
  Yii::app()->clientScript->registerCssFile('/engine/lib/bootstrap/css/bootstrap.min.css');
  //Yii::app()->clientScript->registerCssFile('/engine/lib/bootstrap/css/bootstrap-responsive.min.css');
  Yii::app()->clientScript->registerCssFile('/themes/simple/css/content.css');
  Yii::app()->clientScript->registerCssFile('/themes/simple/css/page.css');
?>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
  <div id="wrap" class="container well">
    <header id="head" class="row">
<?php if (Yii::app()->request->url == "/"){ ?>
      <div class="logo span4"><img border="0" alt="Название компании - На главную" src="/gfx/l0000000.gif"></div>
<?php } else { ?>
      <a href="/" title="Главная страница" class="logo span4"><img src="/gfx/l0000000.gif" alt="Логотип компании"></a>
<?php }?>
      <div class="cname span7">Ваша компания
        <p>«Слоган или вид деятельности»</p>
      </div>
      <div class="tright span5">
        <div class="tel"><span>Телефоны:</span></div>
        <div class="numbers">
          <p>+7 (8212) <strong>29-10-56</strong></p>
          <p>+7 (8212) <strong>39-14-81</strong></p>
        </div>
      </div>
    </header>
    <div id="main">
<?php
  if (false){
?>
      <div class="row">
<?php
     //тут виджеты сверху
?>
      </div>
<?php
  }
?>
      <div class="row">
<?php $this->widget('MenuWidget', array(
  'rootItem' => Yii::app()->menu->all,
  'htmlOptions' => array('class' => 'menu tabs'),
  'labelTemplate' => '<b>{label}</b>',
  'encodeLabel' => false,
)); ?>
      </div>
      <div id="container" class="row">

        <div id="content" class="span10">
          <div class="page-header">
            <h1><?php echo $this->caption; ?></h1>
          </div> 
          
          <?php if(isset($this->breadcrumbs)): // Цепочка навигации ?>
          <?php $this->widget('BreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
          <?php endif?>
          
          <div class="cContent">
             <?php echo $content; ?>
          </div>
        </div>        
        
        <div id="sidebar" class="span5 offset1">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
      </div>
<?php //Тут возможно какие-нить модули снизу ?>
    </div>
    <div id="content_sub" class="row">
      <div class="span14">Copyright &copy;  <?php echo date('Y'); ?> Название компании</div>
    </div>
  </div>
  <div id="footer" class="container">
    <div class="row">
      <div id="mega" class="span6"><span class="copyright" style="font-size:10px;">Сделать сайт в Сыктывкаре: <a title="Создание сайтов" href="http://cvek.ru">веб студия "Цифровой век"</a></span></div>
      <div id="counters" class="span10"></div>
    </div>
  </div>
</div>


</body>
</html>