<?php
Yii::import('application.extensions.urlManager.LangUrlManager');
Yii::import('ygin.behaviors.ActiveRecordTreeBehavior');
class PrTree extends ActiveRecordTreeBehavior  {
  
  
  
  private $_parent = null;
  private $_child = array();
  
  private static $_tree = array();
  
  public function getTree($addCriteria=array(), $cacheKey=null) {
    $cacheKey = $this->owner->tableName();
    if ( isset(self::$_tree[$cacheKey]) ) {
      return self::$_tree[$cacheKey];
    }
    $projectConfig = include $_SERVER['DOCUMENT_ROOT'].'protected/config/project.php';
    $language = $projectConfig['components']['urlManager']['languages'];
    $urlLan = array();
    foreach ($language as $lan)  {
      $urlLan[] = '/'.$lan.'/'; 
    }
    $urlLan[] = '/';
    
    $tree = array();
    if (($cache = Yii::app()->cache) !== null && ($val = $cache->get($cacheKey)) !== false) {
      $tree = $val;
    } else {
      $criteria = new CDbCriteria();
      
      if (!in_array(Yii::app()->request->url, $urlLan) )
        $criteria->select = 't.name_'.Yii::app()->language.',t.visible, t.alias,t.image,t.go_to_type,t.idParent,t.external_link,t.external_link_type,t.id';
        $criteria->order = $this->order;
     
      if ($this->with != null) $criteria->with = $this->with;
      $items = $this->owner->model()->findAll($criteria);
       
      $child = array();
      $countIntems = count($items);
  
      $idParentField = $this->idParentField;
  
      for($i = 0; $i < $countIntems; $i++) {
        $item = $items[$i];
        $id = $item->getPrimaryKey();
        $idParent = $item->$idParentField;
        if ($idParent !== null) {
          $child[$idParent][] = $item;
        }
      }
      $tree = $this->owner->model();
      for ($i = 0; $i < $countIntems; $i++) {
        $item = $items[$i];
        $id = $item->getPrimaryKey();
         
        if (isset($child[$id])) {
          $countChild = count($child[$id]);
          for($k = 0; $k < $countChild; $k++) {
            $child[$id][$k]->setParent($item);
          }
          $item->setChild($child[$id]);
        }
        if ($item->$idParentField === null) {
          $tree->addChild($item);
        }
      }
  
      if ($cache !== null) {
        $cache->set($cacheKey, $tree, 3600);
      }
  
    }
  
    self::$_tree[$cacheKey] = $tree;
    return $tree;
  }
}
