<div class="cDocsList">
  <div class="cDocsCategory">
    <p>Категории документов:</p>
    <p>
<?php
   foreach ($categoryList as $category)  {
     if ($category->countDocs > 0)
       echo CHtml::link($category->name,$category->getUrl()).'&nbsp;&nbsp;';
   }
?>
    </p>
</div>
  
<?php
foreach ($docList as $document)  {
?>
<div class="item cRoundAll">
              <div class="date"><?php echo Yii::app()->dateFormatter->format(Yii::app()->getModule('documents')->getListDateFormat(),$document->date); ?></div>
<?php if ($document->documentFile != null) { ?>
               <a href="<?php echo $document->documentFile->getUrlPath(); ?>"><?php echo $document->name; ?></a>
<?php } else { ?>
               <?php echo $document->name;?>
<?php } ?>
              </div>
<?php
}
?>

</div>
<?php  $this->widget('LinkPagerWidget', array(
  'pages' => $pages,
)); ?>
