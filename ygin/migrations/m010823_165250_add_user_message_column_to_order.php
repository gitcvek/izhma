<?php

class m010823_165250_add_user_message_column_to_order extends CDbMigration
{
  public function safeUp() {
    $this->execute("ALTER TABLE `pr_order` ADD `user_message` text");
  }

  public function safeDown() {
    echo get_class($this)." does not support migration down.\n";
    return false;
  }
}