<?php
$this->registerCssFile('vote-widget.css');
?>
<div class="b-vote-widget span4 pull-right" id="vote_widget_<? echo $voting->id_voting; ?>">
<div class="b-vote-widget-header">Ответьте нам</div>
  <h4><?php echo $voting->name; ?></h4>
<?php
  $this->render($view, array(
    'voting' => $voting,
    'voteCount' => $voteCount,
  ));
?>
</div>