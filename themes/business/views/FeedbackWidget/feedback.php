<div class="b-feedback-widget">
    <?php
    if (strpos(Yii::app()->request->url, '/kmi/') !== false) {
        $bgPath = '/themes/business/gfx/vodzjyr.png';
    } else {
        $bgPath = '/themes/business/gfx/poplavok.png';
    }
    ?>

    <button class="b-feedback-widget__btn" style="background: url('<?php echo $bgPath; ?>') no-repeat;"
            onclick="$('#feedbackForm').modal();">
        &nbsp;
    </button>
    <div class="b-feedback-widget_adresat">
        <?php
        //foreach( $parentAdresat as $curAdresat ) {
        //	echo '<p>' . CHtml::link($curAdresat->name,'#',array(
        //			'rel' => $curAdresat->id_adresat,
        //			'onclick' => "$('#feedbackForm').modal();selectAdresat(" . $curAdresat->id_adresat . ");return false;"
        //		)) . '</p>';
        //}
        //?>
        <div class="b-feedback_control"></div>
    </div>
    <?php

    if (Yii::app()->user->hasFlash('feedback-success')) {
        $this->widget('AlertWidget', array(
            'title' => 'Приемная',
            'message' => Yii::app()->user->getFlash('feedback-success'),
        ));
    }

    $hidden = 'none';
    if (Yii::app()->user->hasFlash("feedback-message")) {
        $hidden = 'block';
        //echo '<i>'.Yii::app()->user->getFlash('feedback-message').'</i>';
    }

    /**
     * @var $form CActiveForm
     */
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'feedbackForm',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'focus' => array($model, 'fio'),
        'htmlOptions' => array(
            'class' => 'modal form-horizontal b-feedback-form',
            'style' => 'display:' . $hidden . ';',
            'enctype' => 'multipart/form-data'
        ),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
        ),
        'errorMessageCssClass' => 'label label-important',
        'action' => Yii::app()->createUrl(FeedbackModule::ROUTE_FEEDBACK),
    ));


    ?>
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>Приемная</h3>
    </div>
    <div class="modal-body b-feedback-form-body">
        <div class="error"><?php echo Yii::app()->user->getFlash("feedback-message"); ?></div>
        <fieldset>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'fio', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'fio', array(
                        'class' => 'input-xlarge focused',
                        'autocomplete' => 'off'
                    )); ?>
                    <?php echo $form->error($model, 'fio'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'themes', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'themes', array(
                        'class' => 'input-xlarge focused',
                        'autocomplete' => 'off'
                    )); ?>
                    <?php echo $form->error($model, 'themes'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'phone', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'phone', array(
                        'class' => 'input-xlarge',
                        'autocomplete' => 'off',
                        'type' => 'tel'
                    )); ?>
                    <?php echo $form->error($model, 'phone'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'mail', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'mail', array(
                        'class' => 'input-xlarge',
                        'autocomplete' => 'off',
                        'type' => 'email'
                    )); ?>
                    <?php echo $form->error($model, 'mail'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'message', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textArea($model, 'message', array(
                        'class' => 'input-xlarge',
                        'rows' => '8',
                        'autocomplete' => 'off'
                    )); ?>
                    <?php echo $form->error($model, 'message'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'id_way', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->dropDownList($model, 'id_way',
                        PrFeedback::getResponseWaysArray(),
                        array('class' => 'input-xlarge focused', 'autocomplete' => 'off')); ?>
                    <?php echo $form->error($model, 'id_way'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'fileupload', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo CHtml::activeFileField($model, 'fileupload'); ?>
                    <?php echo $form->error($model, 'fileupload'); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'adresat', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->dropDownList($model, 'adresat', CHtml::listData($adresats, 'id_adresat', 'name'), array(
                        'class' => 'input-xlarge focused',
                        'autocomplete' => 'off'
                    )); ?>
                    <?php echo $form->error($model, 'adresat'); ?>
                </div>
            </div>
            <?php /* ?><div class="control-group">
				<div class="controls">
					<input type="checkbox" name="agree" id="agree" value="1">
					<label style="display: inline;"><a href="/ru/page/politika/" target="_blank">Я согласен с политикой администрации в отношении обработки персональных данных</a></label>
				</div>
			</div><?php */ ?>
            <div class="control-group">
                <div class="control-group col-lg-12">
                    <label class="col-lg-4"></label>
                    <div class="controls">
                        <div class="g-recaptcha" data-sitekey="<?php echo Yii::app()->reCaptcha->sitekey; ?>"></div>
                    </div>
                </div>
                <?php /*echo $form->labelEx($model,'verifyCode',array('class' => 'control-label')); ?>
				<div class="controls">
					<?php echo $form->textField($model,'verifyCode',array(
						'class' => 'input-mini',
						'title' => 'Укажите код с картинки',
						'autocomplete' => 'off'
					)); ?>
					<?php $this->widget('CCaptcha',array(
						'clickableImage' => true,
						'captchaAction' => FeedbackModule::ROUTE_FEEDBACK_CAPTCHA
					)); ?>
					<?php echo $form->error($model,'verifyCode',array(),true,false); ?>
				</div><?php */ ?>
            </div>
        </fieldset>
    </div>
    <div class="modal-footer">
        <?php echo CHtml::submitButton('Отправить', array('class' => 'btn btn-large btn-primary btn-submit'/*, 'disabled' => 'disabled'*/)); ?>
        <button class="btn" data-dismiss="modal">Отмена</button>
    </div>
    <?php $this->endWidget(); ?>
</div>