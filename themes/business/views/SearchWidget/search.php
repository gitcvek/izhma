<?php 
if (strpos(Yii::app()->request->url, '/kmi/') !== false) {
	$placeholder = 'Сайтысь корсьӧм';
} else {
	$placeholder = 'Поиск по сайту';
}
?>
<form action="<?=Yii::app()->createUrl(SearchModule::ROUTE_SEARCH_VIEW)?>" class="pull-right input-append">
  <input type="text" id="query" name="query" class="span2" onblur="this.placeholder = '<?php echo $placeholder; ?>'" onfocus="this.placeholder = ''" placeholder="<?php echo $placeholder; ?>">
  <button class="btn btn-inverse" type="submit"></button>
</form>