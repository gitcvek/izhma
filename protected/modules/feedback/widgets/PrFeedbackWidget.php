<?php

Yii::import ('ygin.modules.feedback.widgets.FeedbackWidget');
Yii::import ('application.modules.feedback.models.Adresat');
class PrFeedbackWidget extends FeedbackWidget {
	public function run () {
		Yii::app ()->user->setReturnUrl (Yii::app ()->request->url);
		$feedback = Feedback::model ();
		$adresats = Adresat::model ()->findAll (array(
			'order' => 'sequence ASC',
		));
//    $adresat = Adresat::model()->findAll('id_parent IS NOT NULL ORDER BY name ASC');
//    $adresatList = array();
//
//    foreach ($parentAdresat as $parent)  {
//      $controlList = Adresat::model()->findAll(array('condition' => 'id_parent = '.$parent->id_adresat, 'order' => 'name ASC'));
//      if (count($controlList) == 0)  {
//        $adresatList[$parent->id_adresat] =  array($parent);
//      } else {
//        $adresatList[$parent->id_adresat] = $controlList;
//      }
//    }
//    foreach ($adresat as $adres)  {
//      $adresatList[$adres->id_parent][] =  $adres;
//    }
		$this->render ('webroot.themes.business.views.FeedbackWidget.feedback',array(
			'model' => $feedback,
			'adresats' => $adresats,
		));
	}
}