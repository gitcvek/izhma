<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<meta http-equiv="content-language" content="ru">
<?php // TODO - в будущем генетить автоматом ?>
<?php
//Регистрируем файлы скриптов в <head>
if (YII_DEBUG) {
  Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
}
Yii::app()->clientScript->registerCoreScript('jquery');
$this->registerJsFile('bootstrap.min.js', 'ygin.assets.bootstrap.js');
$this->registerJsFile('jquery.cookie.js', 'ygin.assets.js');
$this->registerJsFile('modernizr-2.6.1-respond-1.1.0.min.js', 'ygin.assets.js');

Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/embed.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScriptFile('/themes/business/js/blind.js', CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

Yii::app()->clientScript->registerScript('32', "clicker();", CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('1', "rotator();", CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('42', "clicker1();", CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('43', "rotator1();", CClientScript::POS_READY);
Yii::app()->clientScript->registerScript('2', "slider();", CClientScript::POS_READY);

$this->registerCssFile('bootstrap.min.css', 'webroot.themes.business.bootstrap.css');
// $this->registerCssFile('bootstrap-responsive.min.css', 'ygin.assets.bootstrap.css');
$ass = Yii::getPathOfAlias('ygin.assets.bootstrap.img').DIRECTORY_SEPARATOR;
Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
    $ass.'glyphicons-halflings.png' => '../img/',
    $ass.'glyphicons-halflings-white.png' => '../img/',
    $ass.'glyphicons-halflings-red.png' => '../img/',
    $ass.'glyphicons-halflings-green.png' => '../img/',
));
Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
Yii::app()->clientScript->registerCssFile('/themes/business/css/blind.css');
?>


<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>

<script type="text/javascript">
<!-- //
$(document).ready(function(){

                         fontResizeButtonBind();
                         backroundButtonsBind();

})
//-->
</script>

<div class="topPanel">
  <table cellspacing="0" cellpadding="0">
   <tbody><tr>
     <td style="width: 440px">
     <img src="/themes/business/gfx/blind-non-icon.png" alt="" class="icon-non-blind">
     <a class="link-to-blind" href="#" onclick="versionBlind(); return false;">
					</a><a href="#" onclick="toggleBlind();">Обычная версия</a>
		          
     </td>
     <th>Размер шрифта</th>
     <td class="fontButtons">
       <a class="smallLeterA" href="#">-A</a>
       <a class="bigLeterA" href="#">+A</a></td>
     <th>Цвет фона</th><td class="backgroundButtons">
       <a class="backgroundButton white" href="#">Ц</a>
       <a class="backgroundButton dark" href="#">Ц</a>
       <a class="backgroundButton oringe" href="#">Ц</a>
     </td>
   </tr>
  </tbody></table>
</div>

<?php if (in_array(Yii::app()->request->url, array('/', '/kmi/', '/ru/')) ) { ?>
<body class="main">
<?php } else { ?>
<body>
<?php } ?>

  <div class="main_wrapper">
    <div class="b_top">
      <div>
       
		<a class="blind" href="#" onclick="toggleBlind();">Обычная версия</a>
		
        <?php
 /*         
        $lang = Yii::app()->urlManager->langParam;
        $allLang = Yii::app()->urlManager->languages;
        if(isset($_GET[$lang])&&in_array($_GET[$lang],$allLang) && $_GET[$lang] <> 'ru'){
          $allLang = array_reverse($allLang);
          Yii::app()->language = 'kmi';
          $class = 'russ';
        }
        else  {
          Yii::app()->language = 'ru';
          $class = 'komi';
        }
        if (Yii::app()->request->url == "/") {
          $linkLang = '/'.$allLang[1].'/';
        }
        else  {
          $linkLang = str_replace($allLang[0], $allLang[1], Yii::app()->request->url);
        }
        echo CHtml::link(Yii::t('common', 'Коми кывйöн'),$linkLang,array(
                    'class'=>$class));
  */                  
                    ?>            
      </div>
    </div>

    <div id="wrap" class="container">
      <div id="head" class="row">

 <!--       <div class="i-fon">
          <img class="i-fon__img" src="/themes/business/gfx/pic_fon.png">
        </div>
-->		
		
<?php
  $hostinfo = Yii::app()->request->hostinfo;
  $subDomain = $hostinfo != 'http://www.izhma.ru';
?>
        <?php if (Yii::app()->request->url == "/"){ ?>
        <div class="logo span8">
<?php
  if ($subDomain) {
    echo CHtml::link(CHtml::image('/themes/business/gfx/logo_izhma.png', 'На главную', array('border' => 0)), '/');
  } else {
    echo CHtml::image('/themes/business/gfx/logo_izhma.png', '', array('border' => 0));
  }
?>
        </div>
        <?php } else { ?>
        <div class="logo span8">
<?php 
  if (strpos(Yii::app()->request->url, '/kmi/') !== false) {
    echo CHtml::link(CHtml::image('/themes/business/gfx/logo_izhma.png', 'На главную', array('border' => 0)), '/');
  } else {
    echo CHtml::link(CHtml::image('/themes/business/gfx/logo_izhma.png', 'На главную', array('border' => 0)), '/');
  }
?>
        </div>

        <?php }?>

        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>
		
		
      </div>


      <?php // + Главный блок ?>
      <div id="main">
	  

        <div id="container" class="row">
		        <div class="b-menu-top navbar">
            <?php


$this->widget ('MenuWidget',array(
						'rootItem' => Yii::app ()->menu->all,
						'htmlOptions' => array('class' => 'nav nav-pills navbar-nav'),
						// корневой ul
						'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
						//'itemOptions' => array(), // атрибуты для li
						'itemDropDownOptions' => array('class' => 'dropdown'),
						// все ul кроме корневого
						//'activeCssClass' => 'active',
												// активный li
						//'activateParents' => 'true',
						'labelTemplate' => '{label}', // шаблон для подписи
						'labelDropDownTemplate' => '{label} <b class="caret"></b>',
						// шаблон для подписи разделов, которых есть потомки
						//'linkOptions' => array(), // атрибуты для ссылок
						'linkDropDownOptions' => array(
							'data-target' => '#',
							'class' => 'dropdown-toggle',
							'data-toggle' => 'dropdown'
						),
						// атрибуты для li разделов, у которых есть потомки
						'maxChildLevel' => -1,
						'encodeLabel' => false,
						'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu')
					)
					
					);	

            ?>
			</div>
		
		
		
		
		
          <?php

          $column1 = 0;
          $column2 = 12;
          $column3 = 0;

          if (Yii::app()->menu->current != null) {
            $column1 = 3;
            $column2 = 6;
            $column3 = 3;

            if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {
              $column1 = 0; $column3 = 3;
            }
            if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {
              $column3 = 0; $column1 = $column1*3/3;
            }
            $column2 = 12 - $column1 - $column3;
          }

          ?>
          <?php if ($column1 > 0): // левая колонка ?>
          <div id="sidebarLeft" class="span<?php echo $column1; ?>">
			<div class="search">
		          <?php
          if (Yii::app()->hasModule('search')) {
            $this->widget('SearchWidget');
          } 
          ?>
		  </div>
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
          </div>
          <?php endif ?>

          <div id="content" class="span<?php echo $column2; ?>">
            <div class="page-header">
              <h1>
                <?php echo $this->caption; ?>
              </h1>
            </div>

            <?php if ($this->useBreadcrumbs && isset($this->breadcrumbs)): // Цепочка навигации ?>
            <?php $this->widget('BreadcrumbsWidget', array(
                'homeLink' => array('Главная' => Yii::app()->homeUrl),
                'links' => $this->breadcrumbs,
          )); ?>
            <?php endif; ?>

            <div class="cContent">
                      
<?php 
/* <div class="daGallery">
                   
                    Yii::app()->clientScript->registerScriptFile('/themes/business/views/news/assets/daGallery-min.js', CClientScript::POS_HEAD);
                    if (!($this instanceof PhotogalleryController))
                      Yii::app()->clientScript->registerScript('daGalleryContent.init', "addDaGallery($('.cContent'));", CClientScript::POS_READY);
  </div>*/
 echo $content;
                      ?>
            

              <?php  $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT)); ?>



            </div>

          </div>

          <?php if ($column3 > 0): // левая колонка ?>
          <div id="sidebarRight" class="span<?php echo $column3; ?>">
          					<div class="vklink">
					<a href="http://vk.com/public82279633" target="_blank"><img src="/themes/business/gfx/vklogo.png"></a>
					</div>
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
          </div>
          <?php endif ?>

        </div>
        <?php //Тут возможно какие-нить модули снизу ?>
        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
        <div class="clr"></div>
      </div>
      <?php // - Главный блок ?>

    </div>

    <div class="footer">
      <div id="footer" class="container">
      
        <div class="row">
          <div class="span6">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
          </div>
          <div class="span12">
          <div class="footer-logo">
                              <a href="/">
        <img src="/themes/business/gfx/footer-logo.png">
        </a>
        </div>
        	<div class="footer-text">
            <div id="cvek">
              Создание сайта — веб-студия <a
                title="создать сайт в Цифровом веке" href="http://cvek.ru">&laquo;Цифровой
                век&raquo;</a>
            </div>
            <div id="rights">
            Права на материалы принадлежат официальному сайту администрации МР "Ижемский"<br>
            При использовании материалов сайта активная гиперссылка обязательна.
            </div>
            </div>
          </div><!-- /span12 -->
          
        </div><!-- /row -->
        
      </div>
    </div>
  </div>
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter20986783 = new Ya.Metrika({id:20986783, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/20986783" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>
</html>
