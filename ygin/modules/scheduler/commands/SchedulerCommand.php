<?php

class SchedulerCommand extends CConsoleCommand
{
    /**
     * @var int максимальное количество раз, когда задача может быть завершена ошибочно
     */
    const MAX_COUNT_FAILURES = 10000;
    const ERROR_CODE_LOCK_WAIT_TIMEOUT = 1205;

    protected function setIsolationLevel($level)
    {
        Yii::app()->db
            ->createCommand('SET SESSION TRANSACTION ISOLATION LEVEL ' . $level)
            ->execute();
    }

    public function log($job, $message = null, $isError = true)
    {
        $message = strtr($message, array(
            '{webroot}'  => Yii::getPathOfAlias('webroot'),
            '{name}'     => $job->name,
            '{id}'       => $job->id_job,
            '{time}'     => date('d.m.Y H:i:s', $job->start_date),
            '{failures}' => $job->failures,
            '{max_second_process}' => $job->max_second_process,
        ));

        $msgLevel = CLogger::LEVEL_ERROR;
        if ( !$isError ) $msgLevel = CLogger::LEVEL_INFO;
        Yii::log($message, $msgLevel, 'jobs.scheduler');
    }

    /**
     * @return SchedulerJob
     */
    public function createJob(Job $model)
    {
        Yii::import($model->class_name);
        return Yii::createComponent(array('class' => $model->class_name), $this, $model->id_job);
    }

    /**
     * Если вдруг задача выполняется дольше чем положено, то уведомляем.
     */

    public function checkLongExecuted()
    {
        //Чтобы увидеть дату начала запуска задачи используем грязное чтение данных
        $this->setIsolationLevel('READ UNCOMMITTED');
        $jobs = Job::model()->longExecuted()->findAll();
        $msg = "Планировщик {webroot} с задачей {name}(id: {id}) завис. Время запуска {time}. Ошибок {failures}. Будет сделана попытка перезапустить задачу.";
        foreach ($jobs as $job){
            $this->log($job, $msg);
            $job->start_date = null;
            $job->update(array('start_date'));
        }
    }

    public function actionIndex($args = array())
    {
        // Разблокируем все заблокированные
        $time = time();

        $freezedJobList = Job::model()
            ->resetScope()
            ->freezed(self::MAX_COUNT_FAILURES, $time)
            ->findAll();

        foreach ($freezedJobList as $freezedJob) {
            // Если автоматически разблокируем
            if ($freezedJob->is_auto_unblock) {
                // Задача заблокирована (вероятно, выполняется)
                $freezedJob->start_date = null;
                $freezedJob->save(false);
                $this->log($freezedJob, "Задача планировщика {name} (id={id}) зависла, но была разблокирована автоматически по истечении максимального времени выполнения {max_second_process} сек.", false);
            } else {
                $this->log($freezedJob, "Задача планировщика {name} (id={id}) зависла, время максимального выполнения {max_second_process} сек. истекло.");
            }
        }


        // Обрабатываем задачи по приоритетам
        $candidateCriteria = new CDbCriteria(array('order' => 't.priority DESC, RAND()'));

        $curJobList = Job::model()
                    ->resetScope()
                    ->available(self::MAX_COUNT_FAILURES, $time)
                    ->findAll($candidateCriteria);

        //Нет задач, выходим
        if (count($curJobList) == 0) return;

        foreach ($curJobList as $curJob){

            $curJob->start_date = $time;
            $curJob->last_start_date = $time;
            $curJob->save(false);

            //Создаем и запускаем задачу
            $schedulerJob = $this->createJob($curJob);
            if (!($schedulerJob instanceof SchedulerJob)) {
                $errorMessage = 'Класс задачи {name} ' . get_class($schedulerJob) . ' должен расширять класс SchedulerJob.';
                $this->log($curJob, $errorMessage);
                continue;
            }
            $result = null;
            //try {
                $result = (int)$schedulerJob->run();
            //} catch (Exception $e) {
            //    $curJob->failures++;
            //    $curJob->start_date = null;
            //    $curJob->save(false);
            //    $errorMessage = "Планировщик {webroot} с задачей {name} не отработал, ошибок {failures}.\nОшибка: ".$e->getMessage();
            //    $this->log($curJob,$errorMessage);
            //    continue;
            //}

            // Получаем заново данные, т.к. их могла изменить сама задача
            $curJob = Job::model()->findByPk($curJob->primaryKey);

            $interval = $curJob->interval_value;
            //Если вернулся код ошибки
            if ($result !== SchedulerJob::RESULT_OK) {
                $interval = $curJob->error_repeat_interval;
            }

            $curJob->next_start_date = $curJob->last_start_date + $interval;

            //сохраняем задачу
            if (empty($curJob->first_start_date)) {
                $curJob->first_start_date = $curJob->last_start_date;
            }
            $curJob->start_date = null;
            $curJob->save(false);
        }

    }
}