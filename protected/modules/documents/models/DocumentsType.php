<?php

/**
 * This is the model class for table "pr_documents_type".
 *
 * The followings are the available columns in table 'pr_documents_type':
 * @property integer $id_documents_type
 * @property string $name
 */
class DocumentsType extends DaActiveRecord
{
  
  const ID_OBJECT = 533;
  
  protected $idObject = self::ID_OBJECT;
  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return DocumentsType the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'pr_documents_type';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('name', 'required'),
      array('name', 'length', 'max'=>255),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('id_documents_type, name', 'safe', 'on'=>'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
      'countDocs' => array(self::STAT, 'LinkDocumentType', 'id_documents_type'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_documents_type' => 'Id Documents Type',
      'name' => 'Name',
    );
  }

  public function getUrl() {
    return Yii::app()->createUrl('documents/category', array('id' => $this->id_documents_type));
  }
}
