﻿<div class="mDocsList">
 <h2>Документация</h2>
<?
foreach ($docList as $document)  {
?>
<div class="item cRoundAll">
                <div class="date"><? echo Yii::app()->dateFormatter->format(Yii::app()->getModule('news')->singleNewsDateFormat,$document->date);?></div>
                <?php if ($document->documentFile != null) { ?>
                <h3><a href="<?php echo $document->documentFile->getUrlPath(); ?>"><? echo $document->name;?></a></h3>
                <?php } ?>
              </div>
        <hr class="mDocsList__hr">
<?
}
?>
<div class="archive"><a href="/<?php echo Yii::app()->language;?>/documents/">Все документы</a> →</div>
</div>