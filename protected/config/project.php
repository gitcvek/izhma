<?php
// //////////////////////////
// $user = 'admizhma';
// $password = 'wwhn6lSficLVfsMLRRvQ';
// $dbName = 'admizhma';
// $host = 'localhost';
// /////////////////////////

// $connectionType = (strpos($host, "sock") !== false) ? "unix_socket" : "host";
return array(
    'name' => 'Engine macro',
    'language' => 'ru',
    'theme' => 'business',
    'runtimePath' => dirname(__FILE__) . '/../runtime',
    // используемые в проекте модули
    'modules' => array(
        'documents',
//    'adresat',
        'ygin.comments' => array(
            //'class' => 'ygin.modules.comments.CommentsModule',
            //you may override default config for all connecting models
            'defaultModelConfig' => array(
                //only registered users can post comments
                'registeredOnly' => false,
                'useCaptcha' => false,
                //allow comment tree
                'allowSubcommenting' => true,
                //display comments after moderation
                'premoderate' => false,
                //action for postig comment
                'postCommentAction' => 'comments/comment/postComment',
                //super user condition(display comment list in admin view and automoderate comments)
                'isSuperuser' => 'false',
                //order direction for comments
                'orderComments' => 'ASC',
                //отправлять ли уведомление по комментариям
                'sendNoticeAboutComment' => true,
            ),
            'modelClassMap' => array(
                502 => 'News',
            ),
        ),
        'ygin.news' => array(
            'controllerMap' => array(
                'news' => 'application.modules.news.controllers.PrNewsController',
            ),
        ),
        'ygin.feedback' => array(
            'controllerMap' => array(
                'feedback' => 'application.modules.feedback.controllers.PrFeedbackController',
            ),
        ),
        'ygin.photogallery' => array(
            'controllerMap' => array(
                'photogallery' => 'application.modules.photogallery.controllers.PPhotogalleryController',
            )
        ),
        'ygin.vote' => array(
            'class' => 'application.modules.vote.PVoteModule',
            'controllerMap' => array(
                'voting' => 'application.modules.vote.controllers.PVotingController',
            )
        ),
        'ygin.user' => array(
            'controllerMap' => array(
                'cabinet' => 'application.modules.user.controllers.PCabinetController',
            )
        ),
    ),

    'plugins' => array(
        'ygin.news' => array(
            'class' => 'ygin.modules.news.NewsPlugin',
            /*
            'defaultConfig' => array(
                'models'=>array(
                  'News'=>'application.MyNews',
                ),
            ),*/
        ),
        'ygin.search' => array('class' => 'ygin.modules.search.SearchPlugin'),
        'ygin.feedback' => array('class' => 'ygin.modules.feedback.FeedbackPlugin'),
        'ygin.photogallery' => array('class' => 'ygin.modules.photogallery.PhotogalleryPlugin'),
        'ygin.shop' => array('class' => 'ygin.modules.shop.ShopPlugin'),
        'ygin.faq' => array('class' => 'ygin.modules.faq.FaqPlugin'),
        'ygin.vote' => array('class' => 'ygin.modules.vote.VotePlugin'),
        'ygin.banners' => array('class' => 'ygin.modules.banners.BannerPlugin'),
        'ygin.specoffers' => array('class' => 'ygin.modules.banners.widgets.specialOffer.SpecialOfferPlugin'),
        'ygin.vitrine' => array('class' => 'ygin.widgets.vitrine.VitrinePlugin'),
        'ygin.cloudim' => array('class' => 'ygin.widgets.cloudim.CloudimPlugin'),
        'ygin.review' => array('class' => 'ygin.modules.review.ReviewPlugin'),
        'ygin.user' => array('class' => 'ygin.modules.user.CabinetPlugin'),
        //'ygin.quiz' => array('class' => 'ygin.modules.quiz.QuizPlugin'),
    ),

    // настройки базы данных и проектные компоненты
    'components' => array(
        /*
        'db' => array(
            'connectionString' => 'mysql:' . $connectionType . '=' . $host . ';dbname=' . $dbName,
            'username' => $user,
            'password' => $password,
            'emulatePrepare' => true,
            'charset' => 'utf8',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
        */
        'menu' => array(//        'availableActionParamsOnMain' => array('lang'),
        ),

        'cache' => array(
            'class' => 'system.caching.CFileCache',
        ),

        'log' => array(
            'routes' => array(
                'email_error' => array(
                    'authPassword' => 'xcvew3f,ewr346',
                ),
            ),
        ),
        'clientScript' => array(
            'excludeCssFiles' => array('jquery.fancybox-1.3.4.css'),
            //'compressJs' => false,
        ),
        'urlManager' => array(
            'class' => 'application.components.PrUrlManager',
            'languages' => array('ru', 'kmi'),
            'langParam' => 'lang',
            'rules' => array(
                '' => 'static/page',
            ),
        ),
        'reCaptcha' => array(
      		'class' => 'application.components.ReCaptcha',
      		'sitekey' => '6LdwKi0UAAAAABOf-31hkPSzjLACQz5Pqvz8h0zA',
      		'secretkey' => '6LdwKi0UAAAAAI9iow5nR5jskJNSvBe0vrXubqGX',
        ),
    ),

    'models' => array(
        'News' => 'application.modules.news.models.PrNews',
        'NewsCategory' => 'application.modules.news.models.PrNewsCategory',
        'Menu' => 'application.modules.menu.models.PrMenu',
        'Banner' => 'application.modules.banners.models.PrBanner',
        'Feedback' => 'application.modules.feedback.models.PrFeedback',
    ),

    /*'behaviors' => array(
	   'startUpBehavior' => array(
	       'class' => 'application.components.StartUpBehavior',
	   ),
	),*/

    'import' => array(
        'application.components.cache.*',
    ),

    'onBeginRequest' => create_function('$event', '
       if (strpos(Yii::app()->request->url, "/kmi") !== false) {
         CLocale::$dataPath = $event->sender->basePath."/messages";
       }
       return;
       // $rulesOld = Yii::app()->urlManager->rules;
       // $rules= array();
       // foreach ($rulesOld as $key=>$rul)  {
       //  $rules["<lang:(km|ru)>/".$key] = $rul;
       // }
       //echo print_r($rules,true);
       //  $event->sender->urlManager->addRules($rules);
       //echo print_r( $event->sender->urlManager->rules,true);
       //exit;
       // Yii::app()->urlManager->langParam = "km";
       '),

);

// $connectionType = (strpos($host, "sock") !== false) ?  "unix_socket" : "host";
// return array(
//   'name'=>'Engine macro',
//   'language' => 'ru',
//   'theme' => 'business',
//   'runtimePath' => dirname(__FILE__).'/../runtime',
//   // используемые в проекте модули
//   'modules' => array(
//     'documents',
// //    'adresat',

//     'ygin.comments' =>array(
//       //'class' => 'ygin.modules.comments.CommentsModule',
//       //you may override default config for all connecting models
//       'defaultModelConfig' => array(
//         //only registered users can post comments
//         'registeredOnly' => false,
//         'useCaptcha' => false,
//         //allow comment tree
//         'allowSubcommenting' => true,
//         //display comments after moderation
//         'premoderate' => false,
//         //action for postig comment
//         'postCommentAction' => 'comments/comment/postComment',
//         //super user condition(display comment list in admin view and automoderate comments)
//         'isSuperuser'=>'false',
//         //order direction for comments
//         'orderComments'=>'ASC',
//         //отправлять ли уведомление по комментариям
//         'sendNoticeAboutComment' => true,
//       ),
//       'modelClassMap' => array(
//         502 => 'News',
//       ),
//     ),
//       'ygin.news' => array(
//           'controllerMap' => array(
//               'news' => 'application.modules.news.controllers.PrNewsController',
//           ),
//       ),
//       'ygin.feedback' => array(
//           'controllerMap' => array(
//               'feedback' => 'application.modules.feedback.controllers.PrFeedbackController',
//           ),
//       ),
//       'ygin.photogallery' => array(
//         'controllerMap' => array(
//           'photogallery' => 'application.modules.photogallery.controllers.PPhotogalleryController',
//         )
//       ),
//   ),

//   'plugins' => array(
//       'ygin.news' => array(
//           'class' => 'ygin.modules.news.NewsPlugin',
//           /*
//           'defaultConfig' => array(
//               'models'=>array(
//                 'News'=>'application.MyNews',
//               ),
//           ),*/
//       ),
//       'ygin.search' => array('class' => 'ygin.modules.search.SearchPlugin'),
//       'ygin.feedback' => array('class' => 'ygin.modules.feedback.FeedbackPlugin'),
//       'ygin.photogallery' => array('class' => 'ygin.modules.photogallery.PhotogalleryPlugin'),
//       'ygin.shop' => array('class' => 'ygin.modules.shop.ShopPlugin'),
//       'ygin.faq' => array('class' => 'ygin.modules.faq.FaqPlugin'),
//       'ygin.vote' => array('class' => 'ygin.modules.vote.VotePlugin'),
//       'ygin.banners' => array('class' => 'ygin.modules.banners.BannerPlugin'),
//       'ygin.specoffers' => array('class' => 'ygin.modules.banners.widgets.specialOffer.SpecialOfferPlugin'),
//       'ygin.vitrine' => array('class' => 'ygin.widgets.vitrine.VitrinePlugin'),
//       'ygin.cloudim' => array('class' => 'ygin.widgets.cloudim.CloudimPlugin'),
//       'ygin.review' => array('class' => 'ygin.modules.review.ReviewPlugin'),
//       //'ygin.quiz' => array('class' => 'ygin.modules.quiz.QuizPlugin'),
//   ),

//   // настройки базы данных и проектные компоненты
//   'components'=>array(
//     'db' => array(
//       'connectionString' => 'mysql:'.$connectionType.'='.$host.';dbname='.$dbName,
//       'username' => $user,
//       'password' => $password,
//       'emulatePrepare' => true,
//       'charset' => 'utf8',
//       'schemaCachingDuration'=>3600,
//       'enableProfiling' => true,
//       'enableParamLogging' => true,
//     ),
//     'menu' => array(
// //        'availableActionParamsOnMain' => array('lang'),
//     ),

//     'cache' => array(
//       'class'=>'system.caching.CFileCache',
//     ),

//     'log'=>array(
//         'routes'=>array(
//             'email_error' => array(
//                 'authPassword' => 'xcvew3f,ewr346',
//             ),
//         ),
//     ),
//     'clientScript' => array(
//       'excludeCssFiles' => array('jquery.fancybox-1.3.4.css'),
//       'compressJs' => false,
//     ),
//       'urlManager'=>array(
//           'class'=>'application.components.PrUrlManager',
//           'languages'=>array('ru','kmi'),
//           'langParam'=>'lang',
//           'rules' => array(
//               '' => 'static/page',
//           ),
//       ),
//   ),

//     'models' => array(
//        'News' => 'application.modules.news.models.PrNews',
//        'Menu' => 'application.modules.menu.models.PrMenu',
//        'Banner' => 'application.modules.banners.models.PrBanner',
//        'Feedback' => 'application.modules.feedback.models.PrFeedback',
//     ),

//     'onBeginRequest'=>create_function('$event','
//        if (strpos(Yii::app()->request->url, "/kmi") !== false) {
//          CLocale::$dataPath = $event->sender->basePath."/messages";
//        }
//        return;
//                                                 // $rulesOld = Yii::app()->urlManager->rules;
//                                                 // $rules= array();
//                                                 // foreach ($rulesOld as $key=>$rul)  {
//                                                 //  $rules["<lang:(km|ru)>/".$key] = $rul;
//                                                 // }
//                                                //echo print_r($rules,true);
//                                                //  $event->sender->urlManager->addRules($rules);
//         //echo print_r( $event->sender->urlManager->rules,true);
//         //exit;
//                                                 // Yii::app()->urlManager->langParam = "km";
//                                                  '),

// );
