<?php
class PrMenu extends Menu {
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }
  public function getContent()  {
    
    $criteria = new CDbCriteria();
    
    $criteria->select = 'content_'.Yii::app()->language;
    $criteria->condition = 't.alias = "'.$this->getAttribute('alias').'"';
    
    $content = Menu::model()->find($criteria);

    $pref = 'content_'.Yii::app()->language;
    
    if(strpos(Yii::app()->request->url, 'admin') == true)  {
      
      return $this->getAttribute('content_'.Yii::app()->language);
    }
    else  {
      //return $this->getAttribute('content_'.Yii::app()->language);
      if (!$content) {
        return '';
      }
      return $content->$pref;
    }
  }

  public function rules() {
    $newArray = array();
    $array = parent::rules();
    foreach($array AS $cur) {
      $cur[0] = str_replace('name', 'name_ru', $cur[0]);
      $cur[0] = str_replace('content', 'content_ru', $cur[0]);
      $cur[0] = str_replace('caption', 'caption_ru', $cur[0]);
      $newArray[] = $cur;
    }
    return $newArray;
    return array(
      array('name_ru, id_module_template', 'required'),
      array('visible, id_parent, sequence, handler, go_to_type, id_module_template, external_link_type', 'numerical', 'integerOnly'=>true),
      array('directory', 'length', 'max'=>50),
      array('name_ru, caption_ru, title_teg, meta_description, meta_keywords, external_link', 'length', 'max'=>255),
      array('note', 'length', 'max'=>200),
      array('alias', 'length', 'max'=>100),
      array('content_ru', 'safe'),
    );
  }
    
  public function getName()  {
    return $this->getAttribute('name_'.Yii::app()->language);
  }
  
  public function getZagolovok()  {
   return $this->getAttribute('caption_'.Yii::app()->language);
   return $this->getAttribute('caption');
   
  }
  
  public function getCaption() {
    return $this->getAttribute('caption_'.Yii::app()->language);
    return $this->getAttribute('caption');
  }
  
  public function behaviors() {
    return array(
        'tree' => array(
            'class' => 'ActiveRecordTreeBehavior',
            'order' => 'id_parent DESC, sequence ASC',
            'idParentField' => 'id_parent',
        ),
    );
  }
  
}
