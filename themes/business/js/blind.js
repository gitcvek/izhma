function fontResizeButtonBind() {
  $(".smallLeterA").click(function() {
    var fS = parseInt($("body").css('font-size'));
    $("body").css('font-size', (fS-5)+'px');
    $(".text").css('font-size', (fS-5)+'px');
    $(".b-menu-side-list .nav-list  li  a").css('font-size', (fS-1)+'px');
    $(".news-title-text h3").css('font-size', (fS-2)+'px');
    $.cookie('font_size', (fS-5)+'px', {expires: 365, path:'/'});
    return false;
  });
  $(".bigLeterA").click(function() {
    var fS = parseInt($("body").css('font-size'));
    $("body").css('font-size', (fS+5)+'px');
    $(".text").css('font-size', (fS+5)+'px');
    $(".b-menu-side-list .nav-list  li  a").css('font-size', (fS+1)+'px');
    $(".news-title-text h3").css('font-size', (fS+2)+'px');
    $.cookie('font_size', (fS+5)+'px', {expires: 365, path:'/'});
    return false;
  });
}

function backroundButtonsBind() {
  $(".backgroundButton").click(function() {
    var that = $(this);
    var cssClass = '';
    var theme = null;
    if (that.hasClass('white')) {
      cssClass = '';
      theme = 1;
      $('.logo div a img').attr('src','/gfx/l1.png');
      $('.ver1 img').attr('src','/gfx/ar1.png');
    } else if (that.hasClass('dark')) {
      cssClass = 'bodyBackgroundDark';
      theme = 2;
      $('.logo div a img').attr('src','/gfx/l2.png');
      $('.ver1 img').attr('src','/gfx/dark.png');
    } else if (that.hasClass('oringe')) {
      cssClass = 'bodyBackgroundOringe';
      theme = 3;
      $('.logo div a img').attr('src','/gfx/l1.png');
      $('.ver1 img').attr('src','/gfx/ar1.png');
    }
    $("body").attr('class', cssClass);
    $.cookie('theme', theme, {expires: 365, path:'/'});
    return false;
  });
}

function setPreferences() {
  var theme = $.cookie('theme');
  var fontSize = $.cookie('font_size');
  if (fontSize) {
    $("body").css('font-size', fontSize);
	$(".smallHeader2").css('font-size', fontSize); /*��� ���������� ��������*/
  }
  if (theme) {
    if (theme == 1) {
      cssClass = '';
      $('.logo div a img').attr('src','/gfx/l1.png');
      $('.ver1 .rightAr').attr('src','/gfx/ar1.png');
      $('.ver1 .leftAr').attr('src','/gfx/ar2.png');
         } else if (theme == 2) {
      cssClass = 'bodyBackgroundDark';
      $('.logo div a img').attr('src','/gfx/l2.png');
      $('.ver1 .rightAr').attr('src','/gfx/dark.png');
      $('.ver1 .leftAr').attr('src','/gfx/dark1.png');
         } else if (theme == 3) {
      cssClass = 'bodyBackgroundOringe';
      $('.logo div a img').attr('src','/gfx/l1.png');
      $('.ver1 .rightAr').attr('src','/gfx/ar1.png');
      $('.ver1 .leftAr').attr('src','/gfx/ar2.png');
         }
    $("body").attr('class', cssClass);
  }
}
