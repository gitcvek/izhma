<div class="item cRoundAll">
<div class="news-item-container">
 <!-- <div class="photo"> -->
  <?php if (($preview = $model->getImagePreview('_newsWidget')) !== null): ?>
  	<?php echo CHtml::link(CHtml::image($preview->getUrlPath(), $model->getTitle()), $model->getUrl(), array('title' => $model->getTitle())); ?>
  <?php else:?>
  	<?php echo CHtml::link(CHtml::image(PrNews::EMPTY_IMAGE, $model->getTitle()), $model->getUrl(), array('title' => $model->getTitle())); ?>
  <?php endif; ?>
 <!-- </div>   -->
 <!-- <div class="news-title-text">-->
  
   <span><?php echo CHtml::link($model->getTitle(), $model->getUrl()); ?></span>
 <!-- <div class="text">--><p><?php echo $model->getShort(); ?></p><!--</div> -->
  <div class="more"><?php echo CHtml::link('Полный текст', $model->getUrl()); ?> »</div>
  
<!--  </div> -->
  </div>
  <div class="news-instr">
  <div class="date list_date">
	  <?php echo Yii::app()->dateFormatter->format(Yii::app()->getModule('news')->singleNewsDateFormat, $model->date); ?>
	</div> 
	   <div class="news-img"><a href="<?php echo $model->getUrl()?>#foto"><img src="/themes/business/gfx/icon-picture.png">Фото</a></div>
    <div class="news-comment"><a href="<?php echo $model->getUrl()?>#vk_comments"><img src="/themes/business/gfx/icon-comment.png">Комментарии</a></div>
	  <!--<div class="news-views">
	  	<img src="/themes/business/gfx/icon-eye-open.png"><?php //echo $model->views; ?>
	  	<?php
	  		//Yii::app()->controller->renderDynamic('widget', 'application.modules.news.widgets.countView.CountViewWidget', array('model' => $model), true);
	  	?>
	  </div> -->
	</div>
</div>
