<?php

/**
 * This is the model class for table "pr_documents".
 *
 * The followings are the available columns in table 'pr_documents':
 * @property integer $id_documents
 * @property string $name
 * @property integer $file
 * @property string $date
 */
class Documents extends DaActiveRecord implements ISearchable {

  const ID_OBJECT = 532;
  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Documents the static model class
   */
  public static function model($className=__CLASS__)
  {
    return parent::model($className);
  }


  public function getSearchTitle() {
    return $this->name;
  }
  public function getSearchUrl() {
    //return '/ru/documents/'.$this->primaryKey;
    if ($this->documentFile !== null) {
      return $this->documentFile->getUrlPath();
    }
    return '/ru/documents/' . $this->primaryKey;
  }


  /**
   * @return string the associated database table name
   */
  public function tableName()
  {
    return 'pr_documents';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules()
  {
    // NOTE: you should only define rules for those attributes that
    // will receive user inputs.
    return array(
      array('name, file, date', 'required'),
      array('file', 'numerical', 'integerOnly'=>true),
      array('name', 'length', 'max'=>255),
      array('date', 'length', 'max'=>10),
      // The following rule is used by search().
      // Please remove those attributes that should not be searched.
      array('id_documents, name, file, date', 'safe', 'on'=>'search'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations()
  {
    // NOTE: you may need to adjust the relation name and the related
    // class name for the relations automatically generated below.
    return array(
    'documentFile' => array(
            self::HAS_ONE,
            'File',
            array('id_file' => 'file'),
            'select' => 'id_file, file_path, id_object, id_instance, id_parameter, id_property'
        ),
       'category' => array(self::MANY_MANY,'DocumentsType','pr_link_document_type(id_documents,id_documents_type)'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels()
  {
    return array(
      'id_documents' => 'Id Documents',
      'name' => 'Name',
      'file' => 'File',
      'date' => 'Date',
    );
  }

  protected function beforeDelete() {
    $data = LinkDocumentType::model()->findAll('id_documents='.$this->id_documents);
    foreach($data AS $item) {
      $item->delete();
    }
    return parent::beforeDelete();
  }


}
