<div class="imgWrapper">
<ul>
<?php

$i = 0;
$img= '';
foreach ($this->getNews() as $model)  {
  $style= '';
  if ($model->getContent()) { ?>
  <li <?php if ($i == 0) { echo 'class="cur"'; $style = 'style="display:block"'; }?>>
    <a class="lnk" href="<?php echo $model->getUrl(); ?>" rel="<?php echo $i; ?>">
      <div class="date"><?php echo Yii::app()->dateFormatter->format(Yii::app()->getModule('news')->singleNewsDateFormat, $model->date);?></div>
      <div class="text"><?php echo $model->getTitle(); ?></div>
    </a>
  </li>
<?php
   if ($preview = $model->getImagePreview('_hot'))  {
     $img .= '<a class="img" href="'.$model->getUrl().'" '.$style.'><img src ="'.$preview->getUrlPath().'"></a>';
   }
   else {
     $img .= '<a class="img" href="'.$model->getUrl().'" '.$style.'><img src ="/themes/business/gfx/all_doc_hot.jpg"></a>';
   }
   $i++;
  }
}
?>
</ul>
<?php echo $img; ?>
</div>