function makeZoomOfic() {
$('.to_zoom').jqzoom({
  zoomType: 'innerzoom',
  zoomWidth: 300,
  zoomHeight: 250,
  title: false
});
}

$(document).ready(function(){
  //Зашивон: меняем ссылку в меню для главной страницы
  //if (window.location.pathname.indexOf('kmi')+1) {
    //$("#sidebarLeft>.b-menu-side-list>ul>li:eq(0)>a").attr('href','/kmi/');
  //} else {
    //$("#sidebarLeft>.b-menu-side-list>ul>li:eq(0)>a").attr('href','/');
  //}
  
  
  $('a[href="feedform"]').click(function(){
    $('.b-feedback-widget__btn').click()
    return false;
  });
  $('a[href="ksk"]').click(function(){
    $('#feedbackForm').modal();
    selectAdresat(5);
    return false;
  });
  $('a[href="adm"]').click(function(){
    $('#feedbackForm').modal();
    selectAdresat(1);
    return false;
  });
  $('a[href="sovet"]').click(function(){
    $('#feedbackForm').modal();
    selectAdresat(4);
    return false;
  });
  
  $('.spoiler').each(function(){
    $(this).slideUp();
    var spoilerHead = $(this).find('p:eq(0)');
    spoilerHead.addClass('spoiler-head btn').insertBefore($(this));
  });
  $('.spoiler-head').click(function(){$(this).next().slideToggle()});
  
  $('.modal').on('show', function(){
	$('body').css('overflow', 'hidden');
	var mh = $(this).height();
	if (mh >= $(window).height()){
		$(this).css({'overflow':'scroll-y', 'height': $(window).height()});
	}
  });
  $('.modal').on('hidden', function(){
	  $('body').css('overflow', 'scroll');
  });
  
  $('#agree').click(function() {
	  var btn = $('.btn-submit');
	  
	  if ($(this).prop('checked') == true) {
		  btn.removeAttr('disabled');
	  } else {
		  btn.attr('disabled', 'disabled');
	  }
  });
});

function selectAdresat(id)  {
//  alert('asss');
  $('#feedbackForm .dropAdresat').change(function(){
    $('#b-hidden-adresat').val( $(this).val() );
  });
  $('#feedbackForm .dropAdresat').hide();
  $('#feedbackForm .dropAdresat[rel = '+id+']').show().change();
  $('.modal-header h3').html($('.b-feedback-widget_adresat a[rel="'+id+'"]').html());
   if ($('#feedbackForm .dropAdresat[rel = '+id+'] option').length == 1)  {    
     $('#feedbackForm .dropAdresat[rel = '+id+']').attr({style:"display: inline-block;visibility:hidden"});
     
   }
}

function animateAdresat()  {
  
  if ($('.b-feedback-widget_adresat div:hidden').length > 0) {
    $('.b-feedback-widget_adresat, .b-feedback-widget__btn').animate({"right": "+=265px"}, "slow");
    $('.b-feedback_control').show();
    
    setTimeout(function () {
      if ($('.b-feedback-widget_adresat div:hidden').length == 0)  {
        $('.b-feedback-widget_adresat, .b-feedback-widget__btn').animate({"right": "-=265px"}, "slow");
        $('.b-feedback_control').hide();
        }
    },3000);
  }
  else  {   
    $('.b-feedback-widget_adresat, .b-feedback-widget__btn').animate({"right": "-=265px"}, "slow");
    $('.b-feedback_control').hide();
  }
}

var tm;

function clicker1() {
  var a1;
  var b1;
  $(".b-vitrine a").click( function () {
                  clearInterval(tm);
                  a1 = $(this).attr('rel');
                 // b1 = (a1 != 0) ? "-" + a1 * 986 + "px" : "0px";
         /* if (this == ".b-vitrine a.cur") {
          1+1;
          } else {*/
                  $(".b-vitrine .cur").removeClass("cur");
                  $(this).parent().addClass("cur");
                  //$(".rotator1").animate({"fade" : b1});
                  $(".b-vitrine img:visible").fadeOut(1000);
                  $(".b-vitrine img:eq(" + a1 + ")").fadeIn(1000);
                  rotator1();
                  return false;
                  }
                  //},
                 /* function() {
                    $(".b-vitrine img:eq(" + a1 + ")").css('opacity','1');
                  }*/
                  );
}

function rotator1() {
  var a1;
  tm = setInterval(function () {
                var c1;
                
                c1 = $(".cur a").attr('rel');
                if (c1 != ($('.b-vitrine img').length - 1) ) {
                $(".cur").next().find("a").click();
                }
                else {
                 $(".b-vitrine ul li:first a").click();
                }
                }, 4000);
}

function clicker() {
  var a;
  var b;
  $(".imgWrapper .lnk").hover( function () {
    clearInterval(t);
    a = $(this).attr('rel');
                  //b = (a != 0) ? "-" + a * 986 + "px" : "0px";
    $(".imgWrapper .cur").removeClass("cur");
    $(this).parent().addClass("cur");
                  //$(".rotator").animate({"left" : b});*/
    $(".imgWrapper .img:visible").fadeOut(1000);
    $(".imgWrapper .img:eq(" + a + ")").fadeIn(1000);
    rotator();
    return false;
  },
    function () {
      $(".imgWrapper .img:eq(" + a + ")").css('opacity','1');
    }
  );
}

function rotator() {
  t = setInterval(function () {
    var c;
    c = $(".imgWrapper .cur .lnk").attr('rel');
    if (c != '2') {
      $(".imgWrapper .cur").next().find(".lnk").hover();
    } else {
      $(".imgWrapper ul li:first .lnk").hover();
    }
  }, 6000);
}

function slider() {
  var currentLink = $('a[href="' + window.location.pathname + '"');

  currentLink.siblings('ul').first().css('display', 'block');
  currentLink.addClass('active')
      .parents('ul.js-sub-item-list')
      .css('display', 'block')
      .siblings('a').addClass('active');
}

function addDaGallery(el){
  if (!el) return;
  el.find('img:not(".loader")').each(function(){
//alert($(this).parent()[0].tagName);
    if ($(this).parent()[0].tagName == 'A'){
      //$(this).parent().attr('rel', 'galCont');
       if (( $(this).parent().attr('href').toLowerCase().indexOf('.jpg') > -1 ) ||
           ( $(this).parent().attr('href').toLowerCase().indexOf('.png') > -1 ) ||
           ( $(this).parent().attr('href').toLowerCase().indexOf('.jpeg') > -1 ))
         $(this).parent().attr('rel', "galCont");
    } else {
      $(this).wrap('<a href="'+$(this).attr('src')+'" rel="galCont"></a>');
    }
  });
  DaGallery.init();
}

function toggleBlind () {
	if( $.cookie ( 'blind' ) ) {
			$.cookie ( 'blind',null,{
					expires:1,
					path:'/',
					domain:'.'+location.host
			} );
	}else{
			$.cookie ( 'blind','1',{
					expires:1,
					path:'/',
					domain:'.'+location.host
			} );
	}
	location.reload ();
	return false;
}