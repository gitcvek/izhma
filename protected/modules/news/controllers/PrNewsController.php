<?php
Yii::import ('ygin.modules.news.controllers.NewsController');
Yii::import ('application.modules.news.models.PrNews');
class PrNewsController extends NewsController {
	/**
	* Список новостей
	*/

    public $layout = '//layouts/main';

	public function actionIndex($idCategory = null) {
	    $criteria = new CDbCriteria();
	    $criteria->scopes = array('last');
	    
	    $newsModule = $this->getModule();
	    $category = null;
	    $categories = array();
	    //Если включено отображение категорий
	    if ($newsModule->showCategories) {
	      if ($idCategory !== null && $category = $this->loadModelOr404('NewsCategory', $idCategory)) {
	        $criteria->compare('t.id_news_category', $idCategory);
	      }
	      $categories = NewsCategory::model()->findAll(array('order' => 'seq'));
	    }
	
	    $dependency = PrNews::getDependency();
	    
	    $count = News::model()->cache(3600*24*365, $dependency)->count($criteria);
	    
	    $pages = new CPagination($count);
	    $pages->pageSize = $newsModule->itemsCountPerPage;
	    $pages->applyLimit($criteria);
	    
	    $news = News::model()->cache(3600*24*365, $dependency)->findAll($criteria);
	
	    $this->render('/index', array(
	      'news' => $news,  // список новостей
	      'pages' => $pages,  // пагинатор
	      'category' => $category,  // текущая категория
	      'categories' => $categories,  // все категории
	    ));
	}
	
	/**
	 * Одиночная новость
	 *
	 * @param int $id
	 */
	public function actionView ($id) {
		$model = $this->loadModelOr404('PrNews',$id);
		$this->setKeywords($model->getKeywords ());
		$this->setDescription ($model->getDescription ());
		$model->views += 1;
		$model->scenario = PrNews::SCENARIO_UPDATE_VIEWS;
		$model->save();
        $this->render('/view',array('model' => $model));
	}
	
	public function loadModelOr404($modelClass, $pk, $notFoundMessage = 'Запрашиваемая страница не найдена.') {
	    $dependency = PrNews::getDependency();
	    
	    $model = DaActiveRecord::model($modelClass)->cache(3600*24*365, $dependency)->findByPk($pk);
	    return $this->throw404IfNull($model, $notFoundMessage);
	}
}