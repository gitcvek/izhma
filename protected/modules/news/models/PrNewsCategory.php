<?php

/**
 * This is the model class for table "pr_news_category".
 *
 * The followings are the available columns in table 'pr_news_category':
 * @property integer $id_news_category
 * @property string $name
 * @property integer $seq
 * @property integer $is_visible
 */
class PrNewsCategory extends NewsCategory {
  /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NewsCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'required'),
			array('seq, is_visible', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
      		array('date_create, date_update', 'safe'),
		);
	}
	
	public function behaviors() {
	    $newBehavior = array(
	       'CTimestampBehavior' => array(
	           'class' => 'zii.behaviors.CTimestampBehavior',
	           'createAttribute' => 'date_create',
	           'updateAttribute' => 'date_update',
	           'setUpdateOnCreate' => true,
	       ),
	    );
	    
	    return CMap::mergeArray(parent::behaviors(), $newBehavior);
    }
}