<?php

class PrFeedback extends Feedback
{
    const EMAIL_WAY_ID = 1;
    const POST_WAY_ID = 2;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public $Upload;

    public function attributeLabels()
    {
        return array(
            'id_feedback' => 'Id Feedback',
            'fio' => 'Ф.И.О.',
            'themes' => 'Тема',
            'phone' => 'Телефон, адрес',
            'mail' => 'E-mail',
            'message' => 'Сообщение',
            'date' => 'Дата',
            'ip' => 'Ip',
            'fileupload' => 'прикрепить файл',
            'verifyCode' => 'Код проверки',
            'adresat' => 'Кому отправлять',
            'id_way' => 'Способ получения ответа',
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('fio, message, adresat, themes, id_way', 'required'),
            array('fileupload', 'unsafeExtensions', 'allowEmpty' => true),
            array('mail', 'email', 'message' => 'Введен некорректный e-mail адрес'),
            //array('verifyCode','DaCaptchaValidator','caseSensitive' => true),
            array('fio, phone, mail', 'length', 'max' => 255),
            ['fio', 'match', 'not' => false, 'pattern' => '/[а-яА-Я]/ui'],
            ['mail', 'makeMailRequired'],
            ['phone', 'makePhoneRequired'],
            ['message', 'match', 'not' => true, 'pattern' => '/https?:\/\//ui'],
        );
    }

    public function unsafeExtensions($attribute, $params)
    {
        $file = CUploadedFile::getInstance($this, $attribute);
        // file_put_contents('/usr/www/admizhma.ru/lol.txt', print_r($file, true));
        if (empty($file)) {
            return;
        }
        if (preg_match('/php.*/', $file->getExtensionName())) {
            $this->addError($attribute, 'Недопустимое расширение файла. Ваш IP был записан и администратор уведомлен.');
        }
    }

    public function makeMailRequired($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->id_way == self::EMAIL_WAY_ID) {
                if ($this->$attribute == "") {
                    $this->addError($attribute, "Выбран способ получения ответа через электронную&nbsp; <br/>&nbsp; почту. Необходимо заполнить поле «Электронная почта».");
                }
            }
        }
    }

    public function makePhoneRequired($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->id_way == self::POST_WAY_ID) {
                if ($this->$attribute == "") {
                    $this->addError($attribute, "Выбран способ получения ответа через Почту&nbsp; <br/>&nbsp; России. Необходимо заполнить поле «Телефон, адрес».");
                }
            }
        }
    }

    public function getResponseWaysArray() {
        return [
            self::EMAIL_WAY_ID => 'Электронная почта',
            self::POST_WAY_ID => 'Почта России',
        ];
    }
}
