<?php

class DocumentsModule extends DaWebModuleAbstract
{

  protected $_urlRules = array(
      //'(<lang:(km|ru)>/)?documents' => 'documents/documents/index',
        'documents' => 'documents/documents/index',
        'documents/category/<id:\d+>' => 'documents/documents/category',
  );

  private $_listDateFormat = 'dd MMMM yyyy';

  public function init()
  {
    // this method is called when the module is being created
    // you may place code here to customize the module or the application

    // import the module-level models and components
    $this->setImport(array(
      'documents.models.*',
      'documents.controllers.*',
    ));
  }

  public function getListDateFormat() {
    return $this->_listDateFormat;
  }
}
