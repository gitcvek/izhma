<?php
Yii::import('ygin.modules.news.widgets.news.NewsWidget');
Yii::import ('application.modules.news.models.PrNews');

class MyNewsWidget extends NewsWidget {
	public $id_news_category = null;
	
    public static function getParametersConfig() {
	    return array(
	      'maxNews' => array(
	        'type' => DataType::INT,
	        'default' => 3,
	        'label' => 'Количество отображаемых новостей',
	        'required' => true,
	      ),
	      'id_news_category' => array(
	        'type' => DataType::INT,
	        'default' => 1,
	        'label' => 'ID категории новостей',
	        'required' => true,
	      ),
	    );
    }
    
    public function getNews() {
    	$dependency = PrNews::getDependency();
    	
    	return News::model()->cache(3600*24*365, $dependency)->last($this->maxNews)->findAllByAttributes(array('id_news_category' => $this->id_news_category));
    }
  
    public function init() {
	    if ($this->maxNews === null) {
	      $this->maxNews = 3;
	    }
	    
	    if ($this->id_news_category === null) {
	    	$this->id_news_category = 1;
	    }
	    
	    parent::init();
    }
  
    public function run() {
    	$url = preg_replace("/[^a-z\d]+/", "", Yii::app()->getRequest()->getPathInfo());
    	
    	$this->render('webroot.themes.business.views.NewsWidget.newsWidget', array('url' => $url));
    }
}
?>