<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends DaFrontendController {
   protected function beforeRender($view) {
	  if (isset($_COOKIE['blind'])) {
		  $this->layout = 'webroot.themes.business.views.layouts.blind';
	  }
     Yii::beginProfile('Rendering of controller ' . $this->getUniqueId(), 'profile');
     $result = parent::beforeRender($view);
     return $result;
   }

}