#!/usr/bin/env php
<?php
error_reporting(E_ALL);

/* Permitir al script esperar para conexiones. */
set_time_limit(0);

/* Activar el volcado de salida implícito, así veremos lo que estamo obteniendo
* mientras llega. */
ob_implicit_flush();

$address = '89.223.27.75';
//$address = '127.0.0.1';
$port = 10590;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
    exit;
}

if (socket_bind($sock, $address, $port) === false) {
    echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
    exit;
}

if (socket_listen($sock, 5) === false) {
    echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";
    exit;
}


function sendToApi($action, $params){
    $ch = curl_init();
    $url = 'https://api.securitybox.pro'.$action;
    if (!empty($params)){
        $url .= '?'.http_build_query($params);
    }
    echo "$url\n";
    curl_setopt($ch, CURLOPT_URL, $url);
    //curl_setopt($ch, CURLOPT_HEADER, TRUE);
    //curl_setopt($ch, CURLOPT_NOBODY, TRUE); // remove body
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    //$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    
    return $result;
}

//clients array
$clients = array();
$clientImeiList = [];

do {
    $read = array();
    $read[] = $sock;
   
    $read = array_merge($read,$clients);
    $write = NULL;
    $except = NULL;
    $tv_sec = 5;
    
    // Set up a blocking call to socket_select
    if (socket_select($read, $write, $except, $tv_sec) < 1)
    {
        //    SocketServer::debug("Problem blocking socket_select?");
        continue;
    }
   
    // Handle new Connections
    if (in_array($sock, $read)) {
       
        if (($msgsock = socket_accept($sock)) === false) {
            echo "socket_accept() неудачно: причина: " . socket_strerror(socket_last_error($sock)) . "\n";
            break;
        }
        $clients[] = $msgsock;
        $key = array_keys($clients, $msgsock);
        /* Enviar instrucciones. */
        /*$msg = "\nДобро пожаловать на тестовый сервер PHP. \n" .
        "Вы номер клиента: {$key[0]}.".
        "Чтобы выйти, наберите 'quit'. Закрытие сервера типа 'shutdown'.\n";
        socket_write($msgsock, $msg, strlen($msg));*/
       
    }
   
    // Handle Input
    foreach ($clients as $key => $client) { // for each client
        if (in_array($client, $read)) {
            if (false === ($buf = socket_read($client, 2048))) {
            //if (false === (socket_recvmsg($client, $bufArr))) {
                echo "socket_read() неудачно: причина: " . socket_strerror(socket_last_error($client)) . "\n";
                unset($clients[$key]);
                break 2;
            }
            if (!$buf = trim($buf)) {
                continue;
            }
            if ($buf == 'quit') {
                unset($clients[$key]);
                socket_close($client);
                break;
            }
            if ($buf == 'shutdown') {
                socket_close($client);
                break 2;
            }
            
            if (strpos($buf, '#imei:') !== false){
                $params = explode(" ", $buf);
                if (isset($params[1])){
                    $imei = trim($params[1]);
                    $clientImeiList[$key] = $imei;
                    $action = '/v1/barrier/register/';
                    $params = [
                        'imei' => $imei,
                    ];
                    sendToApi($action, $params);
                }
                
                //$talkback= '#lm';
                //socket_write($client, $talkback, strlen($talkback));
            }
            
            if (strpos($buf, '@lm') !== false){
                $talkback= '#lm';
                socket_write($client, $talkback, strlen($talkback));

                $action = '/v1/barrier/register/';
                $params = [
                    'imei' => $clientImeiList[$key],
                ];
                sendToApi($action, $params);
                //$talkback= '#autent 4321';
                //socket_write($client, $talkback, strlen($talkback));
            }
            
            //$talkback = "Client {$key}: You said '$buf'.\n";
            //socket_write($client, $talkback, strlen($talkback));
            echo "$buf\n";
        }
       
    }
} while (true);

socket_close($sock);
?>