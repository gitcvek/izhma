<?php

//$this->breadcrumbs[$model->title] = $model->getUrl();
$this->setPageTitle($model->title);
$this->useBreadcrumbs = false;

$this->registerJsFile('daGallery-min.js');
Yii::app()->clientScript->registerScript('111', "$('.daGallery a img').parent().attr({rel:'Gall'});;DaGallery.init();", CClientScript::POS_READY);
?>

<?php $this->caption = CHtml::encode($model->getTitle()); ?>

<div class="cNewsSingle">
    <div class="b-news-date">
        <?php echo Yii::app()->dateFormatter->format(Yii::app()
            ->getModule('news')->singleNewsDateFormat, $model->date); ?>
    </div>
    <div class="text">
        <div class="daGallery"><?php echo $model->getContent(); ?></div>
    </div>
    <?php $this->widget('ygin.widgets.likeAndShare.LikeAndShareWidget', array("title" => $model->getTitle())); ?>
    <div class="b-news-archive" id="foto">«&nbsp;<?php echo CHtml::link('Все новости', array('index')); ?></div>
</div>
<div id="vk_comments"></div>
<script type="text/javascript">
    VK.Widgets.Comments("vk_comments", {limit: 10, width: "580", attach: "*"});
</script>
<?php //$this->widget ('ygin.modules.comments.widgets.ECommentsListWidget',array('model' => $model)); ?>
