<?php
	class CountViewWidget extends DaWidget implements IParametersConfig {
		public $model;
		
		public static function getParametersConfig() {
			return array();
		}
		
		public function run() {
	    	$this->render('countView', array('model' => $this->model));
	    }
	}
?>