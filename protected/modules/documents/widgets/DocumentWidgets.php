<?php 
class DocumentWidgets extends DaWidget  {
  public function run() {
    $criteria = new CDbCriteria();
    $criteria->limit  = 8;
    $criteria->order = 'date DESC';
  
    $docList = Documents::model()->findAll($criteria);
    $this->render('documentWidget',array('docList' => $docList));
  }
}

?>
