<?php
class LanguageActiveRecord extends DaActiveRecord
{
  public function __get($name)
  {
    try
    {
      return parent::__get($name . "_" . Yii::app()->getLanguage());
    }
    catch (Exception $exc)
    {
      return parent::__get($name);
    }
  }

  public function getAttributeLabel($attribute)
  {
    $labels = $this->attributeLabels();
    if (isset($labels[$attribute]))
      return $labels[$attribute];
    else if (isset($labels[$attribute . '_' . Yii::app()->getLanguage()]))
      return $labels[$attribute . '_' . Yii::app()->getLanguage()];
    else if (strpos($attribute, '.') !== false)
    {
      $segs = explode('.', $attribute);
      $name = array_pop($segs);
      $model = $this;
      foreach ($segs as $seg)
      {
        $relations = $model->getMetaData()->relations;
        if (isset($relations[$seg]))
          $model = CActiveRecord::model($relations[$seg]->className);
        else
          break;
      }
      return $model->getAttributeLabel($name);
    }
    else
      return $this->generateAttributeLabel($attribute);
  }
}