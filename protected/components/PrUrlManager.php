<?php

Yii::import('application.extensions.urlManager.LangUrlManager', true);

class PrUrlManager extends LangUrlManager  {
  
  protected function createUrlRule($route,$pattern) {    
    //echo $route . "----" . $pattern;
    //echo '<br>';
    //print_r($this->languages);
    $language = $this->languages;         
    //$language[] = '.*?';
    return parent::createUrlRule($route, "<".$this->langParam.":(".implode('|',$language).")>/".$pattern);
  }
  
}