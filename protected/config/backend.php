<?php
$projectConfig = include dirname(__FILE__).'/project.php';

//$defaultConfig = include $_SERVER['DOCUMENT_ROOT'].'ngin/config/mainConfig.php';


$projectConfig['components']['urlManager'] = array(
      'urlFormat'=>'path',
      'showScriptName' => false,
      'urlSuffix' => '/',
      'caseSensitive' => true,
      'rules'=>array(
        'gii'=>'gii',
        'gii/<controller:\w+>'=>'gii/<controller>',
        'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
           
        '<controller:\w+>/<id:\d+>'=>'<controller>/view',
        
        '<controller:\w+>' => '<controller>/index',
        '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
        '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

      ),
    );
$projectConfig['components']['menu'] =array(
    'class' => 'menu.components.DaMenu',
   
    );

$projectConfig['components']['widgetFactory'] = array(
      'class' => 'CWidgetFactory',
      'widgets' => array(
        'ETinyMceYiigin' => array(
          'addAdvancedStyles' => array(
            'Спойлер' => 'spoiler',
          ),
        ),
        'TinymceWidget' => array(
          'options' => array(
	    'addOption' => array(
	      'setup' => 'js:function(ed) {return;ed.onPostProcess.add(function(ed, o) {o.content = cleanCode(o.content, true, false, true, true, true, true, false, true, true);});}',
	    ),
          ),
        ),
      ),
    );


return $projectConfig;
