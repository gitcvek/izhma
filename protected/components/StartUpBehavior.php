<?php
Yii::import('application.components.cache.*');

class StartUpBehavior extends CBehavior
{

    public function events()
    {
        return CMap::mergeArray(parent::events(), array(
            'onBeginRequest' => 'onBeginRequest',
        ));
    }

    public function onBeginRequest($event)
    {

        /* ognev: вызывает ошибку при обращении через консольное приложение, а зачем надо - не ясно
        $request = Yii::app()->request;

        if ($request && $request->url) {
            if (strpos($request->url, '/user/cabinet') !== false) {
                throw new CHttpException(404);
            }
        }*/

        $event->sender->cache->attachBehavior('cacheTaggingBehavior', array(
            'class' => 'CacheTaggingBehavior',
        ));
    }

}